import React from 'react'

function New() {
    
  return (
<div align="center">
<h6>22nd February 2024</h6>
<h1 style={{ textAlign: 'center', fontWeight: 'bold', fontSize: '4em' }}>Sleeve 2.3 — the one <br></br> with the shelves</h1>
<p style={{ textAlign: 'center', fontSize: '2em' }}>We have even more fun visual customizations for <br></br> your themes, alongside interactive progress bars <br></br> and some improvements for the latest versions of <br></br> Apple Music.</p>
<img src='/sleeve 2.3.jpg' alt='Bg' style={{ height: '60%', width: '60%' }} /><br></br><br></br>
<p style={{ textAlign: 'center', fontSize: '1em' }}>Sleeve 2.3 is the first release of the year and a bit of a throwback, to a <br></br> generation of Mac apps that inspired us when we were starting out and <br></br> making things for the Mac many years ago. We've got a few more ideas for <br></br> what to bring back next, but we're starting with one of our favorites.</p>
<h1 style={{ textAlign: 'center', fontWeight: 'bold', fontSize: '3em' }}>New Customization Options</h1>
<h2>Shelves!</h2>
<p style={{ textAlign: 'center', fontSize: '1em' }}>We've wanted to bring some richer theming options to Sleeve for a little <br></br> while now, and we're starting by letting you put Sleeve's album cover on a <br></br> shelf, which naturally comes in a few different finishes.</p>
<img src='/sleeves.jpg' alt='custom' style={{ height: '60%', width: '60%' }} /><br></br><br></br>
<h2>Progress bars!</h2>
<p style={{ textAlign: 'center', fontSize: '1em' }}>While, to be honest, we hadn't received that many requests for shelving, we <br></br> have had plenty for this next one, so we're delighted to announce that in <br></br>  addition to the existing playback controls and track metadata, you can now <br></br>  optionally show a progress bar that tracks the playback position.</p>
<img src='/new custom.jpg' alt='custom' style={{ height: '60%', width: '60%' }} /><br></br><br></br>

</div>


  )
}
export default New
