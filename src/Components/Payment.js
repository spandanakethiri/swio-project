import React, { useState } from 'react';
import axios from 'axios'
import { Button, Modal, ModalHeader, ModalBody,Form,Input,Label,FormGroup,Row,Col } from 'reactstrap';
function Payment(args) {
  const [modal, setModal] = useState(false);
const [formdata, setFormdata] = useState({
    Name:'',
    Amount:''
    
});
const handleInput = (e) =>{
    const {name, value} = e.target
    setFormdata({
        ...formdata,
        [name]:value
    })
}
console.log(formdata)
  const toggle = () => setModal(!modal);
  const handleSubmit = async (e) =>{
    e.preventDefault();
    try{
      console.log(formdata);
      let response = await axios.post(`http://localhost:3000/payment`,formdata)
      console.log(response)
      alert("data inserted");
    }catch (err){
      throw err
    }
   }
  return (
    <div>
      <Button color="danger" onClick={toggle}>
        Payment
      </Button>
      <Modal isOpen={modal} toggle={toggle} {...args}>
        <ModalHeader toggle={toggle}>Payment Gateway</ModalHeader>
        <ModalBody>

<Form onSubmit={handleSubmit}>
  <Row>
    <Col md={6}>
      <FormGroup>
        <Label for="exampleName">
        Name
        </Label>
        <Input
          id="exampleName"
          name="Name"
          placeholder="name"
          type="name"
          value={formdata.Name}
          onChange={handleInput}
          required
        />
      </FormGroup>
    </Col>
    <Col md={6}>
      <FormGroup>
        <Label for="example">
       Amount
        </Label>
        <Input
          id="exampleAmount"
          name="Amount"
          placeholder="Amount"
          type="Amount"
          value={formdata.Amount}
          onChange={handleInput}
          required
        />
      </FormGroup>
    </Col>
  </Row>
  
  
    <Button type='Submit'>
   Submit
    </Button>
  </Form>
        </ModalBody>
      </Modal>
    </div>
  );
}
export default Payment;